using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner2 : MonoBehaviour
{
    public GameObject die;
    Vector2 halfScreen;

    void Start()
    {
        halfScreen = new Vector2(Camera.main.aspect * Camera.main.orthographicSize, Camera.main.orthographicSize);
    }


    void Update()
    {
        if (Time.time == 0)
        {

            Vector2 dieSpawn2 = new Vector2(halfScreen.x - 12.4f, halfScreen.y + 5);
            GameObject newDie2 = (GameObject)Instantiate(die, dieSpawn2, Quaternion.identity);

        }
    }
}
