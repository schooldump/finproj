using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject die;
    Vector2 halfScreen;

    void Start()
    {
        halfScreen = new Vector2(Camera.main.aspect * Camera.main.orthographicSize, Camera.main.orthographicSize);
    }


    void Update()
    {
        if (Time.time == 0)
        {

            Vector2 dieSpawn3 = new Vector2(0, halfScreen.y + 7);
            GameObject newDie3 = (GameObject)Instantiate(die, dieSpawn3, Quaternion.identity);
        }
    }
}
