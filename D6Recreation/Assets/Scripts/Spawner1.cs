using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner1 : MonoBehaviour
{
    public GameObject die;
    Vector2 halfScreen;

    void Start()
    {
        halfScreen = new Vector2(Camera.main.aspect * Camera.main.orthographicSize, Camera.main.orthographicSize);
    }


    void Update()
    {
        if (Time.time == 0)
        {
            Vector2 dieSpawn = new Vector2(halfScreen.x - 100, halfScreen.y + 2);
            GameObject newDie = (GameObject)Instantiate(die, dieSpawn, Quaternion.identity);

        }
    }
}
