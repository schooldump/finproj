using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Die1 : MonoBehaviour
{
    public GameObject rowBox;
    public GameObject lifeP;
    public GameObject die2;
    public GameObject die3;
    public GameObject die4;
    public GameObject die;
    public GameObject selectClickBox;
    public GameObject selectBox;
    private Collider2D boxCol;
    private Collider2D boxCol2;
    private Rigidbody2D boxRb;
    Vector3 pos;
    Vector3 selectPos;
    float speed = 0.2f;
    public bool row;
    bool mouse;
    bool move;
    bool select;
    Rigidbody2D rb;
    Collision2D col;

    void Start()
    {
        GameObject selectClickBox = GameObject.FindWithTag("SelectClick");
        GameObject rowBox = GameObject.FindWithTag("RowBox1");
        GameObject selectBox = GameObject.FindWithTag("Selected");
        GameObject die2 = GameObject.FindWithTag("die2");
        GameObject die3 = GameObject.FindWithTag("die3");
        GameObject die4 = GameObject.FindWithTag("die4");
        GameObject die = GameObject.FindWithTag("Die");
        GameObject boxCol2 = GameObject.FindWithTag("RowBox2");
        boxCol = rowBox.GetComponent<Collider2D>();
        boxRb = rowBox.GetComponent<Rigidbody2D>();
        rb = GetComponent<Rigidbody2D>();
        selectPos = new Vector3(selectBox.transform.position.x, selectBox.transform.position.y, selectBox.transform.position.z);
        Physics2D.IgnoreCollision(die2.GetComponent<Collider2D>(), GetComponent<Collider2D>());
        Physics2D.IgnoreCollision(die3.GetComponent<Collider2D>(), GetComponent<Collider2D>());
        Physics2D.IgnoreCollision(die4.GetComponent<Collider2D>(), GetComponent<Collider2D>());
        Physics2D.IgnoreCollision(die.GetComponent<Collider2D>(), GetComponent<Collider2D>());
        Physics2D.IgnoreCollision(boxCol2.GetComponent<Collider2D>(), GetComponent<Collider2D>());
        Physics2D.IgnoreCollision(selectClickBox.GetComponent<Collider2D>(), GetComponent<Collider2D>());
    }


    void Update()
    {
        //if die is colliding with rowbox, have the mouse hovering over the die, and the mouse is clicked,
        //disable the rowbox collider and switch move boolean to true.
        if (Input.GetMouseButtonDown(0) && row == true && mouse == true)
        {
            boxCol.enabled = !boxCol.enabled;
            move = true;
        }
        //if die is no longer colliding with rowbox, re-enable rowbox collider and set move bool to false.
        else if (row == false)
        {
            boxCol.enabled = true;
            move = false;
        }
        //lerp function to direct die to selection box.
        pos = Vector3.Lerp(transform.position, selectPos, speed);

        //if die is in selection box, mouse is over the die, and mouse is pressed, destroy die object.
        if (select == true && mouse == true && Input.GetMouseButtonDown(0))
        {
            Destroy(gameObject);
            Destroy(lifeP.gameObject);
        }

    }

    private void FixedUpdate()
    {
        //if move boolean is true, move die to selection box position.
        if (move == true)
        {
            rb.MovePosition(pos);
        }
    }

    //mouse over functions to check if mouse is hovering over the die.
    private void OnMouseOver()
    {
        mouse = true;
    }
    private void OnMouseExit()
    {
        mouse = false;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        //If collision is with Selection box, turn selection to true and 
        //row to false. Turning row to false re-enables the rowbox collision.
        GameObject other = collision.gameObject;
        if (other.CompareTag("Selected"))
        {
            select = true;
            row = false;
        }
        //if collision is with rowbox, turn off gravity for die and turn row to true.
        //row boolean indicates the die is colliding with rowbox.
        if (other.CompareTag("RowBox1"))
        {
            rb.gravityScale -= 1f;
            row = true;
        }

    }
/*    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameObject other = collision.gameObject;
        GameObject die2 = GameObject.FindWithTag("die2");
        if (other.CompareTag("die2"))
        {
            print("dfadsfadsf");
            Physics2D.IgnoreCollision(other.GetComponent<Collider2D>(), GetComponent<Collider2D>());
        }
    }*/
}
