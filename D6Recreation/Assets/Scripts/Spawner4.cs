using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner4 : MonoBehaviour
{
    public GameObject die;
    Vector2 halfScreen;

    void Start()
    {
        halfScreen = new Vector2(Camera.main.aspect * Camera.main.orthographicSize, Camera.main.orthographicSize);
    }


    void Update()
    {
        if (Time.time == 0)
        {

            Vector2 dieSpawn5 = new Vector2(halfScreen.x - 1.8f, halfScreen.y + 11);
            GameObject newDie5 = (GameObject)Instantiate(die, dieSpawn5, Quaternion.identity);
        }
    }
}
