using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner3 : MonoBehaviour
{
    public GameObject die;
    Vector2 halfScreen;

    void Start()
    {
        halfScreen = new Vector2(Camera.main.aspect * Camera.main.orthographicSize, Camera.main.orthographicSize);
    }


    void Update()
    {
        if (Time.time == 0)
        {

            Vector2 dieSpawn4 = new Vector2(halfScreen.x - 5.3f, halfScreen.y + 9);
            GameObject newDie4 = (GameObject)Instantiate(die, dieSpawn4, Quaternion.identity);
        }
    }
}
